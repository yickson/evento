<?php

/**
 * Controlador para gestionar la configuración básica del evento
 */
class ConfiguracionController extends AppController
{

  function before_filter()
  {
    View::template('admin');
  }

  public function index()
  {
    $this->evento = (New Configuracion)->find(1);
  }

  public function editar()
  {
    $this->evento = (New Configuracion)->find(1);
  }

  //Métodos AJAX

  public function editar_configuracion()
  {
    $dato = (New Configuracion)->editar();
    $this->data = $dato;
    View::select(null, 'json');
  }

}


?>
