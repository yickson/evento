<?php

/**
 * Controlador para gestionar las transacciones y pagos con webpay
 */
class WebpayController extends AppController
{

  function before_filter()
  {
    View::template('admin');
  }

  public function index()
  {

  }

  //Métodos AJAX
  public function listar_operaciones()
  {
    $dato = (New WebpayTransaccion)->listar();
    $this->data = $dato;
    View::select(null, 'json');
  }

}



?>
