<?php

/**
 * Controller por defecto si no se usa el routes
 *
 */
class IndexController extends AppController
{

    function before_filter()
    {
      View::template('main');
    }

    public function index()
    {
      $this->programa = (New Programa)->find(1);
      $this->config = (New Configuracion)->find(1);
      $this->conf = (New Conferencistas)->find();
    }

    //Método Ajax para mejorar los flujos de interacción

    public function incribir_usuario()
    {
      View::select(null, 'json');
    }
}
