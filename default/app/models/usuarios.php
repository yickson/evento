<?php

/**
 * Modelo para gestionar a los usuarios
 */
class Usuarios extends ActiveRecord
{
  public function inscribir($nombre, $rut, $correo, $telefono, $region, $comuna, $cargo, $colegio, $entrada)
  {
    $usuario = (New Usuarios);
    $usuario->nombre = $nombre;
    $usuario->rut = $rut;
    $usuario->correo = $correo;
    $usuario->telefono = $telefono;
    $usuario->region_id = $region;
    $usuario->comuna_id = $comuna;
    $usuario->cargo_id = $cargo;
    $usuario->colegio = $colegio;
    $usuario->cantidad = $entrada;
    $usuario->fecha = date('Y-m-d H:i:s');

    $evento = (New Configuracion)->find(1);
    if($usuario->cantidad <= $evento->cupos){
      if($usuario->save()){
        Session::set('compra', $usuario->id);
        return 1;
      }else{
        return 2;
      }
    }else{
      return 3;
    }

  }
}



?>
